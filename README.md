# Artwork

<p align="center">
    <a href="https://gitlab.com/src_prepare/artwork/pipelines">
        <img src="https://gitlab.com/src_prepare/badge/badges/master/pipeline.svg">
    </a>
    <a href="https://gentoo.org/">
        <img src="https://gitlab.com/src_prepare/badge/-/raw/master/powered-by-gentoo-linux-tyrian.svg">
    </a>
    <a href="./LICENSE">
        <img src="https://gitlab.com/src_prepare/badge/-/raw/master/license-cc0-1.0-universal-blue.svg">
    </a>
    <a href="https://app.element.io/#/room/#src_prepare:matrix.org">
        <img src="https://gitlab.com/src_prepare/badge/-/raw/master/chat-matrix-green.svg">
    </a>
    <a href="https://gitlab.com/src_prepare/artwork/commits/master.atom">
        <img src="https://gitlab.com/src_prepare/badge/-/raw/master/feed-atom-orange.svg">
    </a>
</p>


# About

This repo contains artwork (wallpapers, icons, logos, misc pictures) created by src_prepare project.


# License

All works licensed to their owners belong to them.
Other than that all works are licensed under a CC0 1.0 Universal license.
