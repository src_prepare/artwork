Licensed under CC-BY-SA/2.5

Based on Gentoo logo vector version dark/mono by Lennart Andre Rolland and Gentoo Foundation, Inc., licensed under CC-BY-SA/2.5

https://wiki.gentoo.org/wiki/Project:Artwork/Artwork#Vector_version_.22g.22_logo
